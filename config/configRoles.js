const path = require('path');

module.exports = {
  "config": path.resolve('./app/config', 'configDatabase.json'),
  "models-path": path.resolve('./app/models'),
  "seeders-path": path.resolve('./app/seeders'),
  "migrations-path": path.resolve('./app/migrations')
};

require('dotenv').config()

module.exports = {
    'secret': process.env.SECRET,
    ROLEs: ['USER', 'ADMIN', 'PM']
};

